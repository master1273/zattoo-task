package paul.chernenko.zattoo.technicaltask.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.reactivex.Maybe
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import paul.chernenko.zattoo.technicaltask.data.ZattooApi
import paul.chernenko.zattoo.technicaltask.data.model.Movie
import paul.chernenko.zattoo.technicaltask.data.model.MovieData
import paul.chernenko.zattoo.technicaltask.data.model.MoviesDataResponse
import paul.chernenko.zattoo.technicaltask.data.model.MoviesResponse
import paul.chernenko.zattoo.technicaltask.utils.LiveDataResult
import java.net.SocketException

@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    lateinit var zattooApi: ZattooApi

    lateinit var mainViewModel: MainViewModel

    // Responses
    private val moviesResponse = MoviesResponse(
        imageBase = "",
        movies = listOf(
            Movie(
                id = 1,
                price = "",
                isAvailable = false,
                imageUrl = "",
                movieData = null
            )
        )
    )

    private val sameIdMoviesDataResponse = MoviesDataResponse(
        movieDataList = listOf(
            MovieData(
                movieId = 1,
                title = "",
                subTitle = ""
            )
        )
    )

    private val notSameIdMoviesDataResponse = MoviesDataResponse(
        movieDataList = listOf(
            MovieData(
                movieId = 2,
                title = "",
                subTitle = ""
            )
        )
    )

    // Tests
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        this.mainViewModel = MainViewModel(this.zattooApi)
    }

    @Test
    fun fetchMovies_positiveResponse_sameIds() {
        `when`(this.zattooApi.getMovieOffers()).thenAnswer {
            return@thenAnswer Maybe.just(moviesResponse)
        }

        `when`(this.zattooApi.getMovieData()).thenAnswer {
            return@thenAnswer Maybe.just(sameIdMoviesDataResponse)
        }

        val observer = mock(Observer::class.java) as Observer<LiveDataResult<List<Movie>>>
        this.mainViewModel.moviesObserver.observeForever(observer)

        this.mainViewModel.fetchMovieData()

        assertNotNull(this.mainViewModel.moviesObserver.value)
        assertTrue(this.mainViewModel.moviesObserver.value!!.data is List<Movie>)
        (this.mainViewModel.moviesObserver.value!!.data as List<Movie>).let {
            assertTrue((it).isNotEmpty())
            assertNotNull(it.first().movieData)
        }
        assertEquals(LiveDataResult.Status.SUCCESS, this.mainViewModel.moviesObserver.value?.status)
    }

    @Test
    fun fetchMovies_positiveResponse_differentIds() {
        `when`(this.zattooApi.getMovieOffers()).thenAnswer {
            return@thenAnswer Maybe.just(moviesResponse)
        }

        `when`(this.zattooApi.getMovieData()).thenAnswer {
            return@thenAnswer Maybe.just(notSameIdMoviesDataResponse)
        }

        val observer = mock(Observer::class.java) as Observer<LiveDataResult<List<Movie>>>
        this.mainViewModel.moviesObserver.observeForever(observer)

        this.mainViewModel.fetchMovieData()

        assertNotNull(this.mainViewModel.moviesObserver.value)
        assertTrue(this.mainViewModel.moviesObserver.value!!.data is List<Movie>)
        (this.mainViewModel.moviesObserver.value!!.data as List<Movie>).let {
            assertTrue((it).isNotEmpty())
            assertNull(it.first().movieData)
        }
        assertEquals(LiveDataResult.Status.SUCCESS, this.mainViewModel.moviesObserver.value?.status)
    }

    @Test
    fun fetchMovieOffers_error_firstRequest() {
        `when`(this.zattooApi.getMovieOffers()).thenAnswer {
            return@thenAnswer Maybe.just(moviesResponse)
        }

        `when`(this.zattooApi.getMovieData()).thenAnswer {
            return@thenAnswer Maybe.error<SocketException>(SocketException())
        }

        val observer = mock(Observer::class.java) as Observer<LiveDataResult<List<Movie>>>
        this.mainViewModel.moviesObserver.observeForever(observer)

        this.mainViewModel.fetchMovieData()

        assertNotNull(this.mainViewModel.moviesObserver.value)
        assertEquals(LiveDataResult.Status.ERROR, this.mainViewModel.moviesObserver.value?.status)
        assert(this.mainViewModel.moviesObserver.value?.err is Throwable)
    }

    @Test
    fun fetchMovieOffers_error_secondRequest() {
        `when`(this.zattooApi.getMovieOffers()).thenAnswer {
            return@thenAnswer Maybe.error<SocketException>(SocketException("Error"))
        }

        `when`(this.zattooApi.getMovieData()).thenAnswer {
            return@thenAnswer Maybe.just(sameIdMoviesDataResponse)
        }

        val observer = mock(Observer::class.java) as Observer<LiveDataResult<List<Movie>>>
        this.mainViewModel.moviesObserver.observeForever(observer)

        this.mainViewModel.fetchMovieData()

        assertNotNull(this.mainViewModel.moviesObserver.value)
        assertEquals(LiveDataResult.Status.ERROR, this.mainViewModel.moviesObserver.value?.status)
        assert(this.mainViewModel.moviesObserver.value?.err is Throwable)
    }

    @Test
    fun setLoadingVisibility_onSuccess() {
        `when`(this.zattooApi.getMovieData()).thenAnswer {
            return@thenAnswer Maybe.just(moviesResponse)
        }

        `when`(this.zattooApi.getMovieOffers()).thenAnswer {
            return@thenAnswer Maybe.just(sameIdMoviesDataResponse)
        }

        val spiedViewModel = spy(this.mainViewModel)

        spiedViewModel.fetchMovieData()
        verify(spiedViewModel, times(2)).setLoadingVisibility(ArgumentMatchers.anyBoolean())
    }

    @Test
    fun setLoadingVisibility_onError() {
        `when`(this.zattooApi.getMovieData()).thenAnswer {
            return@thenAnswer Maybe.error<SocketException>(SocketException())
        }

        `when`(this.zattooApi.getMovieOffers()).thenAnswer {
            return@thenAnswer Maybe.error<SocketException>(SocketException())
        }

        val spiedViewModel = spy(this.mainViewModel)

        spiedViewModel.fetchMovieData()
        verify(spiedViewModel, times(2)).setLoadingVisibility(ArgumentMatchers.anyBoolean())
    }

    @Test
    fun setLoadingVisibility_onNoData() {
        `when`(this.zattooApi.getMovieOffers()).thenReturn(Maybe.create {
            it.onComplete()
        })

        `when`(this.zattooApi.getMovieData()).thenReturn(Maybe.create {
            it.onComplete()
        })

        val spiedViewModel = spy(this.mainViewModel)

        spiedViewModel.fetchMovieData()
        verify(spiedViewModel, times(2)).setLoadingVisibility(ArgumentMatchers.anyBoolean())
    }
}