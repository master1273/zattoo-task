package paul.chernenko.zattoo.technicaltask

import android.app.Application
import android.content.Context
import paul.chernenko.zattoo.technicaltask.data.ApiClient
import paul.chernenko.zattoo.technicaltask.data.ZattooApi

class ZattooApp : Application() {

    companion object {
        @JvmStatic lateinit var instance: ZattooApp
            private set

        @JvmStatic lateinit var apiClient: ZattooApi

        @JvmStatic fun getAppContext() : Context = instance.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        apiClient = ApiClient.getInstance()
    }

}