package paul.chernenko.zattoo.technicaltask.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import paul.chernenko.zattoo.technicaltask.data.ZattooApi
import paul.chernenko.zattoo.technicaltask.data.model.Movie
import paul.chernenko.zattoo.technicaltask.data.model.MoviesDataResponse
import paul.chernenko.zattoo.technicaltask.data.model.MoviesResponse
import paul.chernenko.zattoo.technicaltask.utils.LiveDataResult

class MainViewModel(
    private val zattooApi: ZattooApi
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val moviesObserver = MutableLiveData<LiveDataResult<List<Movie>>>()

    val loadingLiveData = MutableLiveData<Boolean>()

    /**
     * Request movies
     */
    fun fetchMovieData() {
        this.setLoadingVisibility(true)

        compositeDisposable.add(Maybe.zip(
            this.zattooApi.getMovieOffers(),
            this.zattooApi.getMovieData(),
            BiFunction<MoviesResponse, MoviesDataResponse, List<Movie>>{ moviesResponse, moviesDataResponse ->
                return@BiFunction moviesResponse.getMovies().map { movie ->
                    movie.movieData = moviesDataResponse.movieDataList.find { it.movieId == movie.id }
                    movie
                }
            })
            .doOnSubscribe { disposable ->
                this@MainViewModel.moviesObserver.postValue(LiveDataResult.loading())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                // onSuccess
                { movies ->
                    this@MainViewModel.moviesObserver.postValue(LiveDataResult.success(movies))
                    this@MainViewModel.setLoadingVisibility(false)
                },

                //onError
                { throwable ->
                    this@MainViewModel.moviesObserver.postValue(LiveDataResult.error(throwable))
                    this@MainViewModel.setLoadingVisibility(false)
                },

                // onComplete
                {
                    this@MainViewModel.setLoadingVisibility(false)
                }
            )
        )
    }


    /**
     * Set a progress dialog visible on the View
     * @param visible visible or not visible
     */
    fun setLoadingVisibility(visible: Boolean) {
        loadingLiveData.postValue(visible)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
