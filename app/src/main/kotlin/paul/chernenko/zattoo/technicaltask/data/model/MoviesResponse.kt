package paul.chernenko.zattoo.technicaltask.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MoviesResponse (

    @SerializedName("image_base")
    val imageBase: String,

    @SerializedName("movie_offers")
    @Expose
    private val movies: List<Movie>
){

    fun getMovies(): List<Movie>{
        return movies.map { movie ->
            movie.imageUrl = imageBase + movie.imageUrl
            movie
        }
    }
}