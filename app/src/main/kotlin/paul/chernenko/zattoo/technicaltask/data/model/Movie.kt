package paul.chernenko.zattoo.technicaltask.data.model

import com.google.gson.annotations.SerializedName

data class Movie (

    @SerializedName("price")
    val price: String,

    @SerializedName("image")
    var imageUrl: String,

    @SerializedName("available")
    val isAvailable: Boolean,

    @SerializedName("movie_id")
    val id: Int,

    var movieData: MovieData?
)