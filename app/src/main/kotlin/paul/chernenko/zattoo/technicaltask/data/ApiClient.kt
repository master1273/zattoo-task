package paul.chernenko.zattoo.technicaltask.data

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    private var retrofit: Retrofit? = null

    private const val BASE_URL = "https://7f6589e2-26c2-4250-8c0d-f2ff7eb67872.mock.pstmn.io/"

    fun getInstance(): ZattooApi {
        if (retrofit == null)
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(createClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(createGson()))
                .build()
        return retrofit!!.create<ZattooApi>(ZattooApi::class.java)
    }

    private fun createGson(): Gson {
        return GsonBuilder()
            .setDateFormat("MM/dd/yyyy")
            .create()
    }

    private fun createClient(): OkHttpClient {
        return OkHttpClient().newBuilder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
    }
}
