package paul.chernenko.zattoo.technicaltask.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import paul.chernenko.zattoo.technicaltask.data.ZattooApi

class MainViewModelFactory(
    private val zattooApi: ZattooApi
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(this.zattooApi) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}