package paul.chernenko.zattoo.technicaltask.ui.main

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.item_list_movie.view.*
import paul.chernenko.zattoo.technicaltask.R
import paul.chernenko.zattoo.technicaltask.data.model.Movie

class MoviesAdapter(
    private val movies : List<Movie>
) : RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_list_movie, viewGroup, false))
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holer: MovieViewHolder, position: Int) {
        holer.setupItem(movies[position])
    }

    class MovieViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun setupItem(movie: Movie) {

            itemView.apply {
                title.text = movie.movieData?.title ?: context.getString(R.string.no_title)
                sub_title.text = movie.movieData?.subTitle ?: context.getString(R.string.no_subtitle)
                price.text = movie.price

                if (movie.isAvailable) {
                    availability.text = context.getString(R.string.available)
                    availability.setTextColor(Color.GREEN)
                } else {
                    availability.text = context.getString(R.string.not_available)
                    availability.setTextColor(Color.RED)
                }

                Glide
                    .with(itemView)
                    .load(movie.imageUrl)
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(image)
            }
        }
    }
}