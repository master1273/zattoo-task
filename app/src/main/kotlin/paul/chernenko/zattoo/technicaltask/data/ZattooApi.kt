package paul.chernenko.zattoo.technicaltask.data

import io.reactivex.Maybe
import paul.chernenko.zattoo.technicaltask.data.model.MoviesDataResponse
import paul.chernenko.zattoo.technicaltask.data.model.MoviesResponse
import retrofit2.http.GET

interface ZattooApi {

    @GET("movie-offers")
    fun getMovieOffers() : Maybe<MoviesResponse>

    @GET("movie-data")
    fun getMovieData() : Maybe<MoviesDataResponse>
}