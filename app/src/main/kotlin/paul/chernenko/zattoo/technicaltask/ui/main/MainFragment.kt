package paul.chernenko.zattoo.technicaltask.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_fragment.*
import paul.chernenko.zattoo.technicaltask.R
import paul.chernenko.zattoo.technicaltask.ZattooApp
import paul.chernenko.zattoo.technicaltask.data.model.Movie
import paul.chernenko.zattoo.technicaltask.utils.LiveDataResult

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    private val moviesObserver = Observer<LiveDataResult<List<Movie>>> { result ->
        when (result?.status) {
            LiveDataResult.Status.LOADING -> {
                this.viewModel.setLoadingVisibility(true)
            }

            LiveDataResult.Status.ERROR -> {
                showErrorSnackbar()
            }

            LiveDataResult.Status.SUCCESS -> {
                configureList(result.data!!)
            }
        }
    }

    private val loadingObserver = Observer<Boolean> { visible ->
        progress_bar.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // ViewModel factory
        val factory = MainViewModelFactory(ZattooApp.apiClient)

        // Create ViewModel and bind observer
        this.viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        this.viewModel.moviesObserver.observe(this, this.moviesObserver)
        this.viewModel.loadingLiveData.observe(this, this.loadingObserver)

        // Load data
        this.viewModel.fetchMovieData()
    }

    private fun showErrorSnackbar(){
        Snackbar.make(main, getString(R.string.error_loading_list), Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.try_again){
                this.viewModel.fetchMovieData()
            }
            .show()
    }

    private fun configureList(movies: List<Movie>){
        movies_list.layoutManager = LinearLayoutManager(activity)
        movies_list.setHasFixedSize(true)
        movies_list.adapter = MoviesAdapter(movies)
    }

}
