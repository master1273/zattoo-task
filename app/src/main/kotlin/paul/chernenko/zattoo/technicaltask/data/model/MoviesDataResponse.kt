package paul.chernenko.zattoo.technicaltask.data.model

import com.google.gson.annotations.SerializedName

data class MoviesDataResponse (

    @SerializedName("movie_data")
    val movieDataList: List<MovieData>
)