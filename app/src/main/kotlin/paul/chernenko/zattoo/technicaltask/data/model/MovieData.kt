package paul.chernenko.zattoo.technicaltask.data.model

import com.google.gson.annotations.SerializedName

class MovieData (

    @SerializedName("movie_id")
    val movieId: Int,

    @SerializedName("title")
    val title: String,

    @SerializedName("sub_title")
    val subTitle: String
)