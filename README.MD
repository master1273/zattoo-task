## Android technical task ##
We would like you to complete a simple programming task. The goal is to allow you to demonstrate your best programming practices, habits and your approach to common problems. The task should not take more than 4 hours to complete, but take your time if you need more than that. However, please stick to the time frame agreed in the recruitment process.

Feel free to use any libraries that, in your opinion, are appropriate for the task. Please don’t use libraries that solve the whole problem at once, e.g. download and display the whole list of items based just on a provided url. Remember, we want to evaluate your coding skills.

When evaluating your solution, we will focus on application architecture, code quality, unit tests, as well as libraries used.

For your task, please create an app that:

1. Displays a list of movies available at https://7f6589e2-26c2-4250-8c0d-f2ff7eb67872.mock.pstmn.io/movie-offers
2. Furthermore, there is a list of additional data for each movie which should be displayed as part of each movie item, available at https://7f6589e2-26c2-4250-8c0d-f2ff7eb67872.mock.pstmn.io/movie-data
3. You are free to choose a UI approach, as long as it displays the parent and child items in a distinguishable manner

Please provide the solution source code as a zip archive or upload it to a private online repository of your choosing. If possible, use a version control system and have the commit log reflects your progress. Please make sure the code compiles and tests run green. Please send your solution to andrii@zattoo.com. Good luck and happy coding!