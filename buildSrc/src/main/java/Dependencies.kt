object Versions {
    const val min_sdk = 21
    const val target_sdk = 29
    const val compile_sdk = 29

    const val kotlin = "1.3.50"

    const val androidx_appcompat = "1.1.0"
    const val androidx_constraint_layout = "1.1.3"
    const val androidx_ktx = "1.1.0"
    const val androidx_lifecycle = "2.1.0"

    const val android_material = "1.0.0"

    const val rx_android = "2.1.1"
    const val rx_java = "2.2.12"
    const val rx_kotlin = "2.4.0"

    const val retrofit = "2.6.2"

    const val glide = "4.10.0"

    // Tests
    const val junit = "4.12"
    const val arch_core = "2.1.0"
    const val mockito_inline = "2.8.47"
}

object Libs {

    // Kotlin
    const val kotlin_std = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"

    // androidx
    const val androidx_appcompat = "androidx.appcompat:appcompat:${Versions.androidx_appcompat}"
    const val androidx_ktx = "androidx.core:core-ktx:${Versions.androidx_ktx}"
    const val androidx_constraint_layout ="androidx.constraintlayout:constraintlayout:${Versions.androidx_constraint_layout}"

    const val android_material = "com.google.android.material:material:${Versions.android_material}"

    // view model
    const val lifecycle_extentions = "androidx.lifecycle:lifecycle-extensions:${Versions.androidx_lifecycle}"
    const val lifecycle_view_model = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.androidx_lifecycle}"

    // RX android
    const val rx_android = "io.reactivex.rxjava2:rxandroid:${Versions.rx_android}"
    const val rx_java = "io.reactivex.rxjava2:rxjava:${Versions.rx_java}"
    const val rx_kotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rx_kotlin}"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofit_rx_adapter ="com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    const val retrofit_gson_converter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"

    // Glide
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glide_compiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

    // Test
    const val junit = "junit:junit:${Versions.junit}"
    const val arch_core = "androidx.arch.core:core-testing:${Versions.arch_core}"

    const val mockito_inline = "org.mockito:mockito-inline:${Versions.mockito_inline}"
}